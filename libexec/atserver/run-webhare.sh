#!/bin/bash

# short: Run this WebHare

set -eo pipefail

exit_syntax()
{
  echo "Syntax: runkit @server run-webhare [--detach] [--rescue]"
  exit 1
}

DETACH=""
CONTAINER_CMDLINE=()
DOCKEROPTS=()
ASSERVICE=""
REQUIREUNITS=""
NORESTART=""
NOSTART=""

while true; do
  if [ "$1" == "--detach" ]; then
    DETACH="1"
    shift
  elif [ "$1" == "--rescue" ]; then
    CONTAINER_CMDLINE+=("/bin/bash")
    DOCKEROPTS+=(-ti)
    shift
  elif [ "$1" == "--dockeropt" ]; then
    shift
    DOCKEROPTS+=("$1")
    shift
  elif [ "$1" == "--privileged" ]; then
    DOCKEROPTS+=(--privileged)
    shift
  elif [ "$1" == "--requireunit" ]; then
    shift
    REQUIREUNITS="$REQUIREUNITS $1"
    shift
  elif [ "$1" == "--publishrescueport" ]; then
    shift
    [ -n "$1" ] || die "No port specified for --publishrescueport"
    DOCKEROPTS+=(--publish "$1:13679/tcp")
    DOCKEROPTS+=(--env WEBHARE_RESCUEPORT_BINDIP=0.0.0.0)
    shift
  elif [ "$1" == "--as-service" ]; then
    ASSERVICE="1"
    shift
  elif [ "$1" == "--no-restart" ]; then
    NORESTART="1"
    shift
  elif [ "$1" == "--no-start" ]; then
    NOSTART="1"
    shift
  elif [ "$1" == "--help" ]; then
    exit_syntax
  elif [[ "$1" =~ ^-.* ]]; then
    echo "Invalid switch '$1'"
    exit 1
  else
    break
  fi
done

[ -n "$1" ] && exit_syntax

CMDLINE=()

if [ -n "$ASSERVICE" ]; then
  if [ -n "$REQUIREUNITS" ]; then
    systemctl start $REQUIREUNITS # start all of the required units, they may mount the whdata partition
  fi
fi

mkdir -p "$WEBHARE_DATAROOT" #Ensure the dataroot is there

if [ -n "$WHRUNKIT_CONTAINERNAME" ]; then
  configure_runkit_podman

  # when not installing as a service, kill the current container
  [ -z "$ASSERVICE" ] && killcontainer "$WHRUNKIT_CONTAINERNAME"

  USEIMAGE="$(cat "$WHRUNKIT_TARGETDIR"/container.image)"
  # Looks like we have to launch this WebHare using podman

  if [ "$DETACH" == "1" ]; then
    DOCKEROPTS+=(--detach)
  else
    DOCKEROPTS+=(--rm)
  fi

  get_runkit_var NETWORKPREFIX networkprefix
  USEIP="$(cat "$WHRUNKIT_TARGETDIR"/container.ipv4 2>/dev/null || true)"
  if [ -z "$USEIP" ]; then
    (
      [ "$(uname)" == "Darwin" ] || flock -s 200 # No flock on macOS, but we'll take our chances as macOS is not server-production-ready anyway
      # Find a free IP address
      for LASTOCTET in $(seq 2 253) ; do
        ISINUSE=0
        USEIP="${NETWORKPREFIX}.${LASTOCTET}"
        for IP in $(cat "$WHRUNKIT_DATADIR"/*/container.ipv4 2>/dev/null || true); do
          if [ "$IP" == "$USEIP" ]; then
            ISINUSE=1
            break;
          fi
        done
        if [ "$ISINUSE" == "0" ]; then
          break;
        fi
      done
      [ "$ISINUSE" == "0" ] || die "Unable to find a free IP address"
      echo "$USEIP" > "$WHRUNKIT_TARGETDIR"/container.ipv4
    ) 200>"$WHRUNKIT_DATADIR"/.lock
    USEIP="$(cat "$WHRUNKIT_TARGETDIR"/container.ipv4 2>/dev/null || true)"
  fi

 # if [ -z "$WHRUNKIT_TARGETDIR"/container.ipv4 ]; then
#    # Allocate a free IP address under

  if [ "$WHRUNKIT_CONTAINERENGINE" == "docker" ]; then
    CMDLINE+=(docker run -v "$WEBHARE_DATAROOT:/opt/whdata")
  else
    MOUNTFLAGS=""
    if [[ "$OSTYPE" != "darwin"* ]]; then
      # MOUNTFLAGS=":Z" #on darwin this triggers lsetxattr error, see https://github.com/containers/podman-compose/issues/509#issuecomment-1162988103
      DOCKEROPTS+=(--security-opt label=disable)
    else
      DOCKEROPTS+=(--user="$(id -u):$(id -g)" --userns=keep-id)
    fi

    CMDLINE+=(podman run -v "$WEBHARE_DATAROOT:/opt/whdata$MOUNTFLAGS")
  fi

  # --sdnotify=conmon - WH doesn't support NOTIFY_SOCKET yet so a succesful container start will have to do for readyness (https://docs.podman.io/en/v4.4/markdown/options/sdnotify.html)
  if [ -n "$ASSERVICE" ] && [ "$WHRUNKIT_CONTAINERENGINE" == "podman" ]; then
    DOCKEROPTS+=(--sdnotify=conmon)
  fi

  # Added --no-hosts - this easily breaks connectivity to the proxy server if it's aliased to ::1
  CMDLINE+=(-h "$WHRUNKIT_TARGETSERVER".docker
               --network "$WHRUNKIT_NETWORKNAME"
               --ip "$USEIP"
               -e TZ=Europe/Amsterdam
               --label runkittype=webhare
               --log-opt max-size=50m
               --log-opt max-file=5
               --no-hosts
               --name "$WHRUNKIT_CONTAINERNAME"
               "${DOCKEROPTS[@]}"
               "$USEIMAGE"
               "${CONTAINER_CMDLINE[@]}")

else
  CMDLINE=("$WHRUNKIT_WHCOMMAND" console)
fi

if [ -n "$ASSERVICE" ]; then

  configure_runkit_podman

  if ! hash systemctl 2>/dev/null ; then
    echo "No systemctl - cannot install systemd unit"
    exit 1
  fi

# In the interest of server stability, we will not put any runkit code on the webhare-startup path
# TODO using a temp file is nicer
cat > "/etc/systemd/system/$WHRUNKIT_CONTAINERNAME.service" << HERE
# This unitfile was generated by webhare-runkit's run-webhare.sh
[Unit]
Description=WebHare ${WHRUNKIT_TARGETSERVER}
After=${WHRUNKIT_CONTAINERENGINE}.service ${REQUIREUNITS}
Requires=${WHRUNKIT_CONTAINERENGINE}.service ${REQUIREUNITS}

[Service]
TimeoutStartSec=0
Restart=always
Type=notify
NotifyAccess=all

# Ensure any existing container gets out of the way. TODO stop this from making 'not found' noise in the log
ExecStartPre=-"$WHRUNKIT_CONTAINERENGINE" stop $WHRUNKIT_CONTAINERNAME
ExecStartPre=-"$WHRUNKIT_CONTAINERENGINE" rm -f -v $WHRUNKIT_CONTAINERNAME
ExecStart=${CMDLINE[@]}
ExecStartPost=-"$WHRUNKIT_ROOT/bin/runkit" __oncontainerchange started "$WHRUNKIT_CONTAINERNAME"
ExecStopPost=-"$WHRUNKIT_ROOT/bin/runkit" __oncontainerchange stopped "$WHRUNKIT_CONTAINERNAME"
# Tell systemd to use podman (or docker), or it will try to signal conmon which won't understand
ExecStop=-"$WHRUNKIT_CONTAINERENGINE" stop $WHRUNKIT_CONTAINERNAME

[Install]
WantedBy=multi-user.target
HERE

  systemctl daemon-reload
  systemctl enable "$WHRUNKIT_CONTAINERNAME" #ensure autostart
  if [ -z "$NOSTART" ]; then
    if [ -n "$NORESTART" ]; then
      systemctl start "$WHRUNKIT_CONTAINERNAME"
    else
      systemctl restart "$WHRUNKIT_CONTAINERNAME"
    fi
  fi
  exit 0
fi

exec "${CMDLINE[@]}"
